// Initialize Firebase
firebase.initializeApp({
	apiKey: "AIzaSyC3DhiIG7qjMW2Z8lpg_Kn1eAGxUCXsTvY",
	authDomain: "song-l.firebaseapp.com",
	databaseURL: "https://song-l.firebaseio.com",
	projectId: "song-l",
	storageBucket: "song-l.appspot.com",
	messagingSenderId: "132926258699",
	appId: "1:132926258699:web:4bcb96f3d54172d82b910e",
	measurementId: "G-6M5X6WZ42J"
});
firebase.analytics();

function getArtists() {
	var firestore = firebase.firestore();
	var artistsRef = firestore.collection("artists");
	artistsRef = artistsRef.orderBy("name");
	var promise = artistsRef.get();
	return promise.then(function(querySnapshot) {
		var artists = [];
		querySnapshot.forEach(function (doc) {
			var data = Object.assign({id: doc.id}, doc.data());
			artists.push(data);
		});
		return artists;
	}).catch(function(err) {
		console.error(err);
	});
}

function addArtist(name) {
	if (!name) {
		return Promise.reject("Artist name is undefined, null or empty. Not allowed.");
	}
	var firestore = firebase.firestore();
	var fileName = name.toLowerCase();
	fileName = fileName.replace(/[^a-zA-Z]+/g, "").replace(" ", "-");
	return firestore.collection("artists").add({
		name, fileName
	}).then(function(docRef) {
		return docRef.get();
	}).then(function(doc) {
		return Object.assign({id: doc.id}, doc.data());
	}).catch(function(err) {
		console.error(err);
	});
}